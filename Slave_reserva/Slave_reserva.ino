#include <Wire.h>
 // slave

#define endereco 0x08 //endereco para o master referenciar o slave
int t=0;
//int ledVis= 7;
#define sensorVis A1//Antes A2
#define sensortemperatura A0
bool estado=true;
byte comando;
void setup ()
{
    Wire.begin(endereco); 
    Wire.onReceive(escolhas); //funcao que escolhe o comando a ser realizado com base no botao que o usuario apertou
    Wire.onRequest(funcoes); //funcao que realiza o comando com base no valor lido na funcao "escolhas"
    Serial.begin(9600);
    analogReference(DEFAULT); //usa a referencia interna do arduino de 1.1 volts para poder diminuir o "degrau" de leitura 
//    pinMode(ledVis,OUTPUT);
    pinMode(sensorVis,INPUT);
    pinMode(sensortemperatura,INPUT);
}

void loop()
{
   if(estado==true) //vê se os dados chegaram
   {
     estado=false; //avisa que a resposta foi enviada
   }
}

void escolhas(int t )
{
  comando=Wire.read();  //lê o comando enviado pelo master
}
void funcoes() // funcao que seta a funcao com base no valor lido em "comando=Wire.read()"
{   
        if(comando == 1)
        {
           byte calibradoVis[2];
           //digitalWrite(ledVis,HIGH);
           //delay(500000);
           unsigned int valor=analogRead(sensorVis);
           unsigned int auxvalor=valor;
           valor= valor << 8;
           calibradoVis[0]=(valor & 0xFF00) >> 8;
           auxvalor=auxvalor >> 8;
           calibradoVis[1]= auxvalor & 0x00FF;
           //digitalWrite(ledVis,LOW);
           Wire.write(calibradoVis,2);//envia um valor analogico que será convertido no master para ser usado em calculo como uma referencia
        }
        if(comando == 2)
        {  
           byte amostraVis[2];
           //digitalWrite(ledVis,HIGH);
           unsigned int valor=analogRead(sensorVis);
           //delay(5000);
           unsigned int auxvalor=valor;
           valor= valor << 8;
           amostraVis[0]=(valor & 0xFF00) >> 8;
           auxvalor=auxvalor >> 8;
           amostraVis[1]= auxvalor & 0x00FF;
           //digitalWrite(ledVis,LOW);
           Wire.write(amostraVis,2); //envia um valor analogico da amostra já calibrada
        }
        if(comando == 3)
        {
           byte amostraTemp[2];
           unsigned int valor=analogRead(sensortemperatura);
           unsigned int auxvalor=valor;
           valor= valor << 8;
           amostraTemp[0]=(valor & 0xFF00) >> 8;
           auxvalor=auxvalor >> 8;
           amostraTemp[1]= auxvalor & 0x00FF;
           Wire.write(amostraTemp,2); //envia valor analogico lido do sensor para ser convertido no master
        }
}

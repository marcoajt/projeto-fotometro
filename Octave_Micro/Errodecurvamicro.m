function Errodecurvamicro(escolha,inicio,final,nomearqfot,nomearqespec,xtexto,numcoluna,nomefolha)
pkg load io
clc;
disp("\n");
disp("GERANDO GRAFICO DE ERRO A PARTIR DOS SEUS PARAMETROS, CERTIFIQUE-SE DE DIGITAR CORRETAMENTE OS DADOS");
disp("\n");
if escolha==1
titulo=input("Digite o titulo para o grafico:  ","s");
nomedoarquivo2=input("Digite um nome para o arquivo de saida:  ","s");
formato=input("Digite o formato do arquivo de saida (jpg,png,pdf):  ","s");
nomedoarquivo2=strjoin({nomedoarquivo2,formato},'.');
y1=load(nomearqfot); %carrega dados de um arquivo txt
y2=xlsread(nomearqespec,nomefolha);
y2=y2(:,numcoluna);
erro=semilogy(abs(y1-y2),"linewidth",3,'-r'); 
axis([inicio,final,0,1]);
set(erro,"linewidth",2)
set(gca, "linewidth", 2, "fontsize", 15)
xlabel(xtexto);
ylabel('Erro');
title(titulo);
cd Pastadearquivosgerados;
saveas(1,nomedoarquivo2);
pause(5);
close;
clc;
disp("\n");
disp("SEUS ARQUIVOS FORAM GERADOS COM SUCESSO E COLOCADOS NO DIRETORIO 'PASTA DE ARQUIVOS GERADOS'");
disp("\n");
disp("CAMINHO ATE O DIRETORIO COM SEUS ARQUIVOS");
dir=pwd();
dir 
disp("\n");
disp("LISTANDO OS ARQUIVOS DO SEU DIRETORIO");
disp("\n");
ls;
pause(20);
cd ..;
clc;
close;
endif
if escolha ==0
titulo=input("Digite o titulo para o grafico:  ","s");
xtexto=input("Digite um novo nome para o eixo x:  ","s");
inicio=input("Digite um novo valor inicial para a escala do eixo x:  ");
final=input("Digite um novo valor final para a escala do eixo x:  ");
nomedoarquivo2=input("Digite um nome para o arquivo de saida:  ","s");
formato=input("Digite o formato do arquivo de saida (jpg,png,pdf):  ","s");
nomedoarquivo2=strjoin({nomedoarquivo2,formato},'.');
y1=load(nomearqfot); %carrega dados de um arquivo txt
y2=xlsread(nomearqespec,nomefolha);
y2=y2(:,numcoluna);
erro=semilogy(abs(y1-y2),"linewidth",3,'-r'); 
axis([inicio,final,0,1]);
set(erro,"linewidth",2)
set(gca, "linewidth", 2, "fontsize", 15)
xlabel(xtexto);
ylabel('Erro');
title(titulo);
cd Pastadearquivosgerados;
saveas(1,nomedoarquivo2);
pause(5);
close;
clc;
disp("\n");
disp("SEUS ARQUIVOS FORAM GERADOS COM SUCESSO E COLOCADOS NO DIRETORIO 'PASTA DE ARQUIVOS GERADOS'");
disp("\n");
disp("CAMINHO ATE O DIRETORIO COM SEUS ARQUIVOS");
disp("\n");
dir=pwd(); 
dir
disp("\n");
disp("LISTANDO OS ARQUIVOS DO SEU DIRETORIO");
disp("\n");
ls;
pause(20);
cd ..;
clc;
close;
endif
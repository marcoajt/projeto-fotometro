pkg load io
clc;
disp("\n");
disp("SE FAZ NECESSARIO O PACOTE I/O DO OCTAVE PARA QUE SEJA GERADOS OS GRAFICOS");
disp("NO TERMINAL NAO SAO ACEITOS CARACTERES ESPECIAS");
disp("OS NUMEROS DEVEM SER DIGITADOS COM PONTO AO INVES DE VIRGULA (QUANDO SE FIZER NECESSARIO)");
disp("OS ARQUIVOS A SEREM UTILIZADOS NO PROGRAMA DEVEM ESTAR NO SEGUINTE CAMINHO");
pwd()
disp("\n");
disp("UTILIZE O NOME DE UM ARQUIVO LISTADO ABAIXO PARA PODER CARREGAR ELE ADEQUADAMENTE");
disp("\n");
ls;
disp("\n");
disp("GERANDO GRAFICO DA ANALISE A PARTIR DOS SEUS PARAMETROS, CERTIFIQUE-SE DE DIGITAR CORRETAMENTE OS DADOS");
disp("\n");
comando = yes_or_no("GOSTARIA DE TRABALHAR COM DADOS DE APENAS UM GRAFICO?  ");
if comando ==1
disp("\n");
disp("DIGITE O NOME DA SUA PLANILHA DE DADOS");
disp("\n");  
nomearqespec=input("Digite o nome do arquivo com dados a ser lido:  ","s");
nomearqespec=strjoin ({nomearqespec,'xlsx'}, '.');
nomefolha=input("Digite o nome das folhas em sua planilha com os dados do espectrofotometro a ser lido:  ","s");
numcoluna=input("Digite o numero da coluna aonde se encontra os dados de absorbancia:  ");
numcoluna2=input("Digite o numero da coluna aonde se encontra outro dado relevante:  ");
Dados=xlsread(nomearqespec,nomefolha);
y=Dados(:,numcoluna);
x=Dados(:,numcoluna2);
disp("\n");
xtexto=input("Digite o nome do eixo x:  ","s");
ytexto=input("Digite o nome do eixo y:  ","s");
titulo=input("Digite o titulo para o grafico:  ","s");
nomedoarquivo1=input("Digite um nome para o arquivo de saida:  ","s");
formato=input("Digite o formato do arquivo de saida (jpg,png,pdf):  ","s");
nomedoarquivo1=strjoin({nomedoarquivo1,formato},'.');
absorbancia=plot(x,y,'*k');
axis("auto");
set(absorbancia,"linewidth",7)
set(gca, "linewidth", 2, "fontsize", 15)
grid minor on;
xlabel(xtexto);
ylabel(ytexto);
title(titulo);
teste=isdir('Pastadearquivosgerados');
if teste==0
Pastadearquivosgerados=pwd();
mkdir Pastadearquivosgerados;
endif
cd Pastadearquivosgerados;
saveas(1,nomedoarquivo1);
pause(5);
clc;
close;
cd ..; 
endif
if comando==0
nomearqfot=input("Digite o nome do arquivo com dados do fotometro a ser lido:  ","s");
nomearqfot=strjoin ({nomearqfot,'txt'}, '.');
y1=load(nomearqfot); %carrega dados de um arquivo txt
disp("\n");
escolha = yes_or_no("SUA PLANILHA POSSUI MAIS DE UMA FOLHA DE DADOS?:  ");
if escolha==0
disp("\n");
op=yes_or_no("Deseja que os dados sejam plotado em apenas um grafico? (Responder 'no' gera um grafico com apenas dados do espectrotofotometro e outro com dados do fotometro):  ");
if op==1
disp("\n");
disp("DIGITE O NOME DA SUA PLANILHA DE DADOS DO ESPECTROFOTOMETRO");
disp("\n");  
nomearqespec=input("Digite o nome do arquivo com dados do espectrofotometro a ser lido:  ","s");
nomearqespec=strjoin ({nomearqespec,'xlsx'}, '.');
nomefolha=input("Digite o nome das folhas em sua planilha com os dados do espectrofotometro a ser lido:  ","s");
numcoluna=input("Digite o numero da coluna aonde se encontra os dados de absorbancia:  ");
y2=xlsread(nomearqespec,nomefolha);
y2=y2(:,numcoluna);
disp("\n");
xtexto=input("Digite o nome do eixo x:  ","s");
ytexto=input("Digite o nome do eixo y:  ","s");
titulo=input("Digite o titulo para o grafico:  ","s");
inicio=input("Digite o valor inicial da escala do eixo x:  ");
final=input("Digite o valor final da escala do eixo x:  ");
nomedoarquivo1=input("Digite um nome para o arquivo de saida:  ","s");
formato=input("Digite o formato do arquivo de saida (jpg,png,pdf):  ","s");
nomedoarquivo1=strjoin({nomedoarquivo1,formato},'.');
absorbancia=plot(y1,'+r',y2,'*k');
axis([inicio,final,0,1]);
set(absorbancia,"linewidth",7)
set(gca, "linewidth", 2, "fontsize", 15)
grid minor on;
xlabel(xtexto);
ylabel(ytexto);
h=legend("fotometro","espectrofotometro");
legend(h,"location","northeastoutside");
title(titulo);
teste=isdir('Pastadearquivosgerados');
if teste==0
Pastadearquivosgerados=pwd();
mkdir Pastadearquivosgerados;
endif
cd Pastadearquivosgerados;
saveas(1,nomedoarquivo1);
pause(5);
clc;
close;
cd ..;
%chama a funcao Erro de curva micro
disp("\n");
escolha = yes_or_no("GOSTARIA DE UTILIZAR A MESMA ESCALA E O MESMO NOME DO EIXO X ANTERIOR PARA GERAR O GRAFICO DE ERRO?:  ")
if escolha==1
Errodecurvamicro(escolha,inicio,final,nomearqfot,nomearqespec,xtexto,numcoluna,nomefolha)
else
Errodecurvamicro(escolha,inicio,final,nomearqfot,nomearqespec,xtexto,numcoluna,nomefolha)
endif 
else %realiza a plotagem dos graficos separadamente
disp("\n");
disp("DIGITE O NOME DA SUA PLANILHA DE DADOS DO ESPECTROFOTOMETRO");
disp("\n");
nomearqespec=input("Digite o nome do arquivo com dados do espectrofotometro a ser lido:  ","s");
nomearqespec=strjoin ({nomearqespec,'xlsx'}, '.');
nomefolha=input("Digite o nome das folhas em sua planilha com os dados do espectrofotometro a ser lido:  ","s");
numcoluna=input("Digite o numero da coluna aonde se encontra os dados da absorbancia:  ");
y2=xlsread(nomearqespec,nomefolha);
y2=y2(:,numcoluna);
disp("\n");
xtexto=input("Digite o nome do eixo x para o grafico do fotometro:  ","s");
ytexto=input("Digite o nome do eixo y para o grafico do fotometro:  ","s");
titulo=input("Digite o titulo para o grafico do fotometro:  ","s");
inicio=input("Digite o valor inicial da escala do eixo x para o grafico do fotometro:  ");
final=input("Digite o valor final da escala do eixo x para o grafico do fotometro:  ");
fot=input("Digite um nome para o arquivo de saida para o grafico do fotometro:  ","s");
formato=input("Digite o formato do arquivo de saida (jpg,png,pdf) para o grafico do fotometro:  ","s");
fot=strjoin({fot,formato},'.');
fot1=plot(y1,'+r');
axis([inicio,final,0,1]);
set(fot1,"linewidth",7)
set(gca, "linewidth", 2, "fontsize", 15)
grid minor on;
xlabel(xtexto);
ylabel(ytexto);
title(titulo);
teste=isdir('Pastadearquivosgerados');
if teste==0
Pastadearquivosgerados=pwd();
mkdir Pastadearquivosgerados;
endif
cd Pastadearquivosgerados;
saveas(1,fot);
pause(5);
clc;
close;
%trecho para plotar o grafico do espectrofotometro separadamente
xtexto=input("Digite o nome do eixo x para o grafico do espectrofotometro:  ","s");
ytexto=input("Digite o nome do eixo y para o grafico do espectrofotometro:  ","s");
titulo=input("Digite o titulo para o grafico do espectrofotometro:  ","s");
inicio=input("Digite o valor inicial da escala do eixo x para o grafico do espectrofotometro:  ");
final=input("Digite o valor final da escala do eixo x para o grafico do espectrofotometro:  ");
espec=input("Digite um nome para o arquivo de saida para o grafico do espectrofotometro:  ","s");
formato=input("Digite o formato do arquivo de saida (jpg,png,pdf) para o grafico do espectrofotometro:  ","s");
espec=strjoin({espec,formato},'.');
espec1=plot(y2,'+r');
axis([inicio,final,0,1]);
set(espec1,"linewidth",7)
set(gca, "linewidth", 2, "fontsize", 15)
grid minor on;
xlabel(xtexto);
ylabel(ytexto);
title(titulo);
saveas(1,espec);
pause(5);
clc;
close;
cd ..;
%chama a funcao Erro de curva micr
disp("\n");
escolha = yes_or_no("GOSTARIA DE UTILIZAR A MESMA ESCALA E O MESMO NOME DO EIXO X ANTERIOR PARA GERAR O GRAFICO DE ERRO?:  ")
if escolha==1
Errodecurvamicro(escolha,inicio,final,nomearqfot,nomearqespec,xtexto,numcoluna,nomefolha)
else
Errodecurvamicro(escolha,inicio,final,nomearqfot,nomearqespec,xtexto,numcoluna,nomefolha)
endif
endif%fim de teste op
endif %fim de teste escolha
%comeco do trecho de codigo para quando se tem mais de uma folha de dados
op=yes_or_no("Deseja que os dados sejam plotado em apenas um grafico? (Responder 'no' gera um grafico com apenas dados do espectrotofotometro e outro com dados do fotometro):  ");
quantfolha=input("Digite o numero de folhas do seu arquivo:  ");
if escolha==1
if op==1
nomearqespec=input("Digite o nome do arquivo com dados do espectrofotometro a ser lido:  ","s");
nomearqespec=strjoin ({nomearqespec,'xlsx'}, '.');
for i=1:quantfolha
nomefolha=input("Digite o nome das folhas em sua planilha com os dados do espectrofotometro a ser lido:  ","s");
numcoluna=input("Digite o numero da coluna aonde se encontra os dados da absorbancia:  ");
y2=xlsread(nomearqespec,[nomefolha,num2str(i)]);
y2=y2(:,numcoluna);
disp("\n");
xtexto=input("Digite o nome do eixo x:  ","s");
ytexto=input("Digite o nome do eixo y:  ","s");
titulo=input("Digite o titulo para o grafico:  ","s");
inicio=input("Digite o valor inicial da escala do eixo x:  ");
final=input("Digite o valor final da escala do eixo x:  ");
nomedoarquivo1=input("Digite um nome para o arquivo de saida:  ","s");
formato=input("Digite o formato do arquivo de saida (jpg,png,pdf):  ","s");
nomedoarquivo1=strjoin({nomedoarquivo1,formato},'.');
absorbancia=plot(y1,'+r',y2,'*k');
axis([inicio,final,0,1]);
set(absorbancia,"linewidth",7)
set(gca, "linewidth", 2, "fontsize", 15)
grid minor on;
xlabel(xtexto);
ylabel(ytexto);
h=legend("fotometro","espectrofotometro");
legend(h,"location","northeastoutside");
title(titulo);
teste=isdir('Pastadearquivosgerados');
if teste==0
Pastadearquivosgerados=pwd();
mkdir Pastadearquivosgerados;
endif
cd Pastadearquivosgerados;
saveas(1,nomedoarquivo1);
pause(5);
clc;
close;
cd ..;
%chama a funcao Erro de curva micr
disp("\n");
nomefolha=strjoin({nomefolha,num2str(i)},'');
escolha = yes_or_no("GOSTARIA DE UTILIZAR A MESMA ESCALA E O MESMO NOME DO EIXO X ANTERIOR PARA GERAR O GRAFICO DE ERRO?:  ")
if escolha==1
Errodecurvamicro(escolha,inicio,final,nomearqfot,nomearqespec,xtexto,numcoluna,nomefolha)
else
Errodecurvamicro(escolha,inicio,final,nomearqfot,nomearqespec,xtexto,numcoluna,nomefolha)
endif
endfor
endif
if op==0
disp("\n");
disp("DIGITE O NOME DA SUA PLANILHA DE DADOS DO ESPECTROFOTOMETRO");
disp("\n");
nomearqespec=input("Digite o nome do arquivo com dados do espectrofotometro a ser lido:  ","s");
nomearqespec=strjoin ({nomearqespec,'xlsx'}, '.');
for i=1:quantfolha
nomefolha=input("Digite o nome das folhas em sua planilha com os dados do espectrofotometro a ser lido:  ","s");
numcoluna=input("Digite o numero da coluna aonde se encontra os dados da absorbancia:  ");
y2=xlsread(nomearqespec,[nomefolha,num2str(i)]);
y2=y2(:,numcoluna);
disp("\n");
xtexto=input("Digite o nome do eixo x para o grafico do fotometro:  ","s");
ytexto=input("Digite o nome do eixo y para o grafico do fotometro:  ","s");
titulo=input("Digite o titulo para o grafico do fotometro:  ","s");
inicio=input("Digite o valor inicial da escala do eixo x para o grafico do fotometro:  ");
final=input("Digite o valor final da escala do eixo x para o grafico do fotometro:  ");
fot=input("Digite um nome para o arquivo de saida para o grafico do fotometro:  ","s");
formato=input("Digite o formato do arquivo de saida (jpg,png,pdf) para o grafico do fotometro:  ","s");
fot=strjoin({fot,formato},'.');
fot1=plot(y1,'+r');
axis([inicio,final,0,1]);
set(fot1,"linewidth",7)
set(gca, "linewidth", 2, "fontsize", 15)
grid minor on;
xlabel(xtexto);
ylabel(ytexto);
title(titulo);
teste=isdir('Pastadearquivosgerados');
if teste==0
Pastadearquivosgerados=pwd();
mkdir Pastadearquivosgerados;
endif
cd Pastadearquivosgerados;
saveas(1,fot);
pause(5);
clc;
close;
%trecho para plotar o grafico do espectrofotometro separadamente
xtexto=input("Digite o nome do eixo x para o grafico do espectrofotometro:  ","s");
ytexto=input("Digite o nome do eixo y para o grafico do espectrofotometro:  ","s");
titulo=input("Digite o titulo para o grafico do espectrofotometro:  ","s");
inicio=input("Digite o valor inicial da escala do eixo x para o grafico do espectrofotometro:  ");
final=input("Digite o valor final da escala do eixo x para o grafico do espectrofotometro:  ");
espec=input("Digite um nome para o arquivo de saida para o grafico do espectrofotometro:  ","s");
formato=input("Digite o formato do arquivo de saida (jpg,png,pdf) para o grafico do espectrofotometro:  ","s");
espec=strjoin({espec,formato},'.');
espec1=plot(y2,'+r');
axis([inicio,final,0,1]);
set(espec1,"linewidth",7)
set(gca, "linewidth", 2, "fontsize", 15)
grid minor on;
xlabel(xtexto);
ylabel(ytexto);
title(titulo);
teste=isdir('Pastadearquivosgerados');
saveas(1,espec);
pause(5);
clc;
close;
cd ..;
%chama a funcao Erro de curva micr
disp("\n");
nomefolha=strjoin({nomefolha,num2str(i)},'');
escolha = yes_or_no("GOSTARIA DE UTILIZAR A MESMA ESCALA E O MESMO NOME DO EIXO X ANTERIOR PARA GERAR O GRAFICO DE ERRO?:  ")
if escolha==1
Errodecurvamicro(escolha,inicio,final,nomearqfot,nomearqespec,xtexto,numcoluna,nomefolha)
else
Errodecurvamicro(escolha,inicio,final,nomearqfot,nomearqespec,xtexto,numcoluna,nomefolha)
endif
endfor
endif%fim de teste op
endif
endif
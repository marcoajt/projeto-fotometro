#include <math.h>
#include <LiquidCrystal.h>
#include <Wire.h>
#include <string.h>

//master //com4
#define endereco 0x08 //endereco para refenciar o slave
#define botao1 8 //define as entradas que lerao o teclado
#define botao2 9
#define botao3 13
#define botao4 10
LiquidCrystal lcd(12, 11, 5, 4, 3, 2); 
//valor a ser usado como referencia nos calculos
volatile float calibradoVis=0;
int cont=0;
#define conversao 0.48828125 //fator de conversao a ser utilizado para converter um valor analogico em uma grandeza fisica ou quimica

void setup()
{
    Wire.begin(endereco);
    Serial.begin(9600);
    lcd.begin(16, 2);
    pinMode(botao1, INPUT_PULLUP);
    pinMode(botao2, INPUT_PULLUP);
    pinMode(botao3, INPUT_PULLUP);
    pinMode(botao4, INPUT_PULLUP);
}
 
void loop()
{
     int botao1pressionado=digitalRead(botao1); //realiza a leitura constantemente do botoes do teclado
     int botao2pressionado=digitalRead(botao2);
     int botao3pressionado=digitalRead(botao3);
     int botao4pressionado=digitalRead(botao4);
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Aguardando...");
     delay(1000);
     if(!botao1pressionado) //funcao a ser realizada quando o botao 1 é pressionado
     {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Calibrando Vis");
          delay(3000);
          byte comando=1;
          Wire.beginTransmission(endereco);
          Wire.write(comando); // envia o valor 5 para o slave que irá retornar um valor analogico da amostra calibrada que será usada nas analises IR
          button1(); //chama a funcao que irá converter os dados analogicos para o valor de calibração
          Wire.endTransmission();
     }
     if(!botao2pressionado) 
     {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Analise no Vis");
          delay(3000);
          byte comando=2;
          Wire.beginTransmission(endereco);
          Wire.write(comando); //envia 3 para o slave que irá chamar a funcao que retorna valores analogicos de analises no infravermelho
          button2(); //chama a funcao que irá tratar os dados analogicos enviados pelo slave 
          Wire.endTransmission();
     }
     if(!botao3pressionado)
     {  
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Calculando");
          lcd.setCursor(0,1);
          lcd.print("Temperatura");
          delay(3000);
          byte comando=3;
          Wire.beginTransmission(endereco);
          Wire.write(comando); //envia 4 para o slave que irá chamar a funcao que retorna valores analogicos da temperatura
          button3(); //chama a funcao que trata o valor analogico da temperatura
          Wire.endTransmission();
     }
     if(!botao4pressionado) 
     {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Nada aqui");
          delay(3000);
     }
}

void button1() //chama a funcao que irá converter os dados analogicos para um valor de referencia calibrado para analises no IR
{
     byte calibrado[2];
     Wire.requestFrom(endereco,2);
     int i=0;
     while(Wire.available()>0)
     {  
        calibrado[i]=Wire.read();
        delay(100);
        i++;
        if(i==2)
        {
          i=0;   
        }
     }
     unsigned int valor= calibrado[1];
     valor=valor << 8;
     valor=valor|calibrado[0];
     calibradoVis=valor;
     if(cont==0)
     { //nao mostra o valor calibrado1)*c caso esse seja igual a 0, pois não há valores nulos para calibração, pede-se para precisar o botao 3 afim de realizar nova tentativa de calibração
       lcd.clear();
       lcd.setCursor(0,0);
       lcd.print("Aperte 1");
       lcd.setCursor(0,1);
       lcd.print("novamente");
       delay(3000);
     }
     if(cont>0)
      {//mostra o valor calibrado caso esse maior que 0
       lcd.clear();
       lcd.setCursor(0,0);
       lcd.print("Vis calibrado");
       lcd.setCursor(0,1);
       lcd.print(calibradoVis); 
       delay(5000);
      }
      cont++;
}

void button2() //funcao que trata dos valores a respeito dos dados das analises no infravermelho e realiza basicamente os mesmos calculos que a funcao acima
{
     Wire.requestFrom(endereco,4);
     byte calibrado[2];
     Wire.requestFrom(endereco,2);
     int i=0;
     while(Wire.available()>0)
     {  
        calibrado[i]=Wire.read();
        delay(100);
        i++;
        if(i==2)
        {
          i=0;   
        }
     }
     unsigned int valor= calibrado[1];
     valor=valor << 8;
     valor=valor|calibrado[0];
     Serial.println(calibradoVis);
     float transmitanciaVis=(valor/calibradoVis)*100;
     float absorbanciaVis=-log(valor/calibradoVis);
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Transmitancia Vis");
     lcd.setCursor(0,1);
     lcd.print(transmitanciaVis);
     lcd.setCursor(6,1);
     lcd.print("%");
     delay(5000);
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Absorbancia Vis");
     lcd.setCursor(0,1);
     lcd.print(absorbanciaVis);
     delay(5000);
}

void button3() //funcao que trata dos dados analogicos a respeito da temperatura
{
       
          byte calibrado[2];
          Wire.requestFrom(endereco,2);
          int i=0;
          while(Wire.available()>0)
          {  
              calibrado[i]=Wire.read();
              delay(100);
              i++;
              if(i==2)
              {
                 i=0;   
              }
          }
     unsigned int valor= calibrado[1];
     valor=valor << 8;
     valor=valor|calibrado[0];
          float temp=(((valor*5)/(1023))/0.1); 
          if(temp==0)
          { //descarta valores de temperatura que sejam iguais a zero e tenta reler o valor do sensor
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("Aperte 4");
            lcd.setCursor(0,1);
            lcd.print("novamente");
            delay(3000);
          }
          if(temp>0)
          { //mostra os valores de temperatura caso esses sejam maior que 0
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("Temperatura");
            lcd.setCursor(0,1);
            lcd.print(temp);
            lcd.setCursor(7,1);
            lcd.print("Celsius");
            delay(5000);
          }
}
